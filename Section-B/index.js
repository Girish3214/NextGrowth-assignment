let index = 0;
let totalCount = 0;
const lastDiv = document.getElementById("last__element");
const container = document.getElementById("items__container");

function addItems() {
  for (let i = 0; i < 30; i++) {
    const item = document.createElement("div");
    item.classList.add("item");
    item.innerText = `Card ${totalCount + 1}`;
    container.appendChild(item);
    totalCount++;
  }
}

function intersectionCallback(entries, observer) {
  for (const entry of entries) {
    if (entry.isIntersecting) {
      addItems();
    }
  }
}
const observer = new IntersectionObserver(intersectionCallback, {
  threshold: 0.8,
});
observer.observe(lastDiv);

addItems();
